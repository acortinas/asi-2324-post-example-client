import HTTP from "@/common/http";

const resource = "tags";

export default {
  async findAll() {
    return (await HTTP.get(resource)).data;
  },
};
