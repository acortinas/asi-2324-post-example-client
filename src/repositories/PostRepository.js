import HTTP from "@/common/http";

const resource = "posts";

function applyDate(post) {
  post.timestamp = new Date(post.timestamp);
  return post;
}

export default {
  async findAll(query, sort) {
    const params = new URLSearchParams();
    if (query) params.append("query", query);
    if (sort) params.append("sort", sort);
    const paramsStr = params.toString();
    let url = resource;
    if (paramsStr) url += "?" + paramsStr;
    const response = await HTTP.get(url);
    response.data.forEach(applyDate);
    return response.data;
  },

  async findOne(id) {
    return applyDate((await HTTP.get(`${resource}/${id}`)).data);
  },

  async save(post) {
    if (post.id) {
      return applyDate((await HTTP.put(`${resource}/${post.id}`, post)).data);
    } else {
      return applyDate((await HTTP.post(`${resource}`, post)).data);
    }
  },

  async delete(id) {
    return await HTTP.delete(`${resource}/${id}`);
  },
};
